<?php
//trier le json par ordre alphabétique des clés

/* ATTENTION NE PAS APPLIQUER A fr-FR : MAUVAIS CHARSET UTF = RISQUE DE PERTE DE DONNEES */

$chemin_fichier = './en-GB.JSON';

$contenu_fichier = file_get_contents($chemin_fichier);
$tableau_associatif = json_decode($contenu_fichier, true);

ksort($tableau_associatif);

$nouveau_contenu = json_encode($tableau_associatif, JSON_PRETTY_PRINT);
file_put_contents($chemin_fichier, $nouveau_contenu);
?>
