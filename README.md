# oMailgw - UI (html/js)

**Lire la documentation général : https://framagit.org/omailgw/omailgw-doc**

**Ce qui se trouve ci-dessous ne traite que de la partie interface utilisateur**

Offre et support commercial assuré par retzo.net : https://retzo.net/services/hebergement/passerelle-e-mail-relai-smtp/

## Pré-requis

* Un serveur HTTP (lighttpd/apache/nginx)
* [omailgw-api](https://framagit.org/omailgw/omailgw-api/) publié/fonctionnelle

## Installation de oMailgw-ui

Récupération des sources

```bash
cd /var/www
git clone https://framagit.org/omailgw/omailgw.git
cd omailgw
```

Copier le fichier js/settings.js_default en js/settings.js et éditer le, pour notamment, indiquer l'URL de l'API :

```diff
const userSettings = {
  "ui_defaultlanguage": "fr-FR",
-  "api_url": "http://yoururl-omailgw-api.net/api/",
+  "api_url": "https://omailgw-demo.retzo.net/api/",
```

## Utilisateurs/rôles

https://framagit.org/omailgw/omailgw-api/#utilisateursr%C3%B4les

## Capture d'écran

Toutes les captures : https://framagit.org/omailgw/omailgw-doc/-/tree/main/screenshot

Le dashboard

![Dashboard](https://framagit.org/omailgw/omailgw-doc/-/raw/main/screenshot/dashboard.png)

Affichage du tableau de trafic log :

![Trafic Log](https://framagit.org/omailgw/omailgw-doc/-/raw/main/screenshot/logtable.png)

Affichage d'un e-mail

![Trafic Log](https://framagit.org/omailgw/omailgw-doc/-/raw/main/screenshot/one.png)

Gestion des "transports" (routage e-mail)

![Trafic Log](https://framagit.org/omailgw/omailgw-doc/-/raw/main/screenshot/transport.png)

## Démonstration

API de démonstration :

* URL : https://omailgw-demo.retzo.net/
* Utilisateur :

  * root@retzo.net (Rôle root)

  * admin@retzo.net (Rôle admin)

  * user@retzo.net (Rôle : user)
* Mot de passe  : password

## Projets utilisés

* Thème SB Admin 2 : https://startbootstrap.com/theme/sb-admin-2
* DataTable : https://datatables.net/

## Auteurs

* [David Mercereau](https://david.mercereau.info/) [@kepon](https://framagit.org/kepon)
* [Pierrick Anceaux](https://github.com/Shurtugl/) [@Shurtugl](https://framagit.org/Shurtugl)

## Licence

Licence GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

Voir Licence.txt
