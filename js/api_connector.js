;

/**
 * Checks if the user is logged in by :
 *
 * -looking for a cookie.
 *
 * -looking for a session.
 *
 * Also creates a session if a cookie is found (rememberMe)
 *
 * @returns {Promise<boolean>} True if the user is logged in, false otherwise.
 */
export function IsLoggedIn() {
  // Check if token cookie exist to set it and the name up in session
  debug("Verifying login cookie ...");
  const oMailgwToken = document.cookie.split("; ").find((cookie) => cookie.startsWith("oMailgwToken="));
  //check cookie token
  if (oMailgwToken) {
    debug("We have a cookie !");
    const token = oMailgwToken.split("=")[1];
    localStorage.setItem("token", token);
    const oMailgwUser = document.cookie.split("; ").find((cookie) => cookie.startsWith("oMailgwUser="));
    const oMailgwRole = document.cookie.split("; ").find((cookie) => cookie.startsWith("oMailgwRole="));
    //check cookie name
    if (oMailgwUser) { //double security as having a token means we have a user
      const user = oMailgwUser.split("=")[1];
      debug("Setting up user from cookie in session ( " + user + " )");
      localStorage.setItem("userName", user);
    }
    //check cookie role
    if (oMailgwRole) { //double security as having a token means we have a user
      const role = oMailgwRole.split("=")[1];
      debug("Setting up role from cookie in session ( " + role + " )");
      localStorage.setItem("userRole", role);
    }
  } else {
    debug("No cookie found.");
  }
  debug("Verifying login session ...");
  // Having a token in session means we are logged in
  // Security note : a wrong token might suffice and feel like logged in UI but API would respond 401-unauthorized to any request
  const loggedin = localStorage.getItem("token") !== null && localStorage.getItem("token") !== undefined;
  if (loggedin) {
    debug("We have " + localStorage.getItem("userRole") + " in session : " + localStorage.getItem("userName"));
  } else {
    debug("No user found in session.");
  }
  return loggedin;
}

//TODO cypher password
/**
 * Allows a user to log in to the API. It requires strings for email & password, and boolean for "remember me" option.
 *
 * It returns an array elements, [true] if login is a success. [false, api_message, error_type] otherwise
 *
 * @param {string} email the email used as credential
 * @param {string} password the _clear_ password
 * @param {boolean} rememberMe the status of checkbox as boolean
 *
 * @returns {Promise<Array>}
 */
export async function Login(email, password, rememberMe) {
  debug("Asking api to log in as \"" + email + "\" ...");
  debug("api url = " + settings.api_url + " following route : " + settings.api_login);

  const bodyContent = JSON.stringify({
    email,
    password,
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_login,
      type: "POST",
      data: bodyContent,
      headers: settings.headersList,
    });
    debug(response);
    let token = response.token;
    let role = response.role;
    localStorage.setItem("token", token);
    localStorage.setItem("userName", email);
    localStorage.setItem("userRole", role);
    debug("api_connector : login successful");

    if (rememberMe) {
      debug("Checked remember me, giving you a free cookie ! :-)");
      const expires = new Date(
        Date.now() + settings.cookie_duration_days * 24 * 60 * 60 * 1000
      ).toUTCString();
      document.cookie =
        "oMailgwToken=" + token + "; expires=" + expires + " SameSite=Strict";
      document.cookie =
        "oMailgwUser=" + email + "; expires=" + expires + " SameSite=Strict";
      document.cookie =
      "oMailgwRole=" + role + "; expires=" + expires + " SameSite=Strict";
    }
    return [true];

  } catch (error) {
    debug("api_connector : register failed : \"" + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}


/**
 * Logs out the currently logged.
 *
 * @returns {Promise<boolean>} `true` if the logout was successful, `false` otherwise.
 */
export async function Logout() {
  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  document.cookie = "oMailgwToken=remove; expires=Thu, 01 Jan 1970 00:00:00 UTC; SameSite=Strict"
  document.cookie = "oMailgwUser=remove; expires=Thu, 01 Jan 1970 00:00:00 UTC; SameSite=Strict"
  document.cookie = "oMailgwRole=remove; expires=Thu, 01 Jan 1970 00:00:00 UTC; SameSite=Strict"
  localStorage.clear();
  debug("Logged out browser-side, telling API..")
  debug("api url = " + settings.api_url + " following route : " + settings.api_logout);

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_logout,
      type: "POST",
      headers: headers
    });
    debug("api_connector : logout successful");
    return [true];
  } catch (error) {
    debug("api_connector : logout failed : \"" + error.statusText + "\". Response's xhr object :");
    debug(error)
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, error.status, msgFromAPI];
  }

}


//TODO cypher password here & API side
//TODO add api_register to settings
/**
 * Registers a new user. API checks if the email provided already exists in 'from' logs
 *
 * @param {string} email the email used as credential
 * @param {string} password the _clear_ password
 *
 * @returns {Promise<boolean>} `true` if the registration was successful, `false` otherwise.
 */
export async function Register(email, password) {
  debug("registering " + email + " to API :")
  debug("api url = " + settings.api_url + " following route : " + settings.api_register);

  const bodyContent = JSON.stringify({
    "email": email,
    "password": password
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_register,
      type: "POST",
      data: bodyContent,
      headers: settings.headersList
    });

    debug("api_connector : register successful : " + email);
    return [true];
  } catch (error) {
    debug("api_connector : register failed : \"" + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}


/**
 *
 * @param {string} newpassword
 * @returns
 */
export async function ChangePassword(currentPassword, newPassword) {
  debug("Asking API to change pass of " + localStorage.getItem("userName"));
  debug("api url = " + settings.api_url + " following route : " + settings.api_changepassword);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  const bodyContent = JSON.stringify({
    "old_password": currentPassword,
    "new_password": newPassword
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_changepassword,
      method: "POST",
      data: bodyContent,
      headers: headers
    });

    debug("api_connector : changed password successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : password change failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

/**
 *
 * @param {*} frequency
 * @param {*} verbose
 * @returns
 */
export async function GetSettings() {
  debug("Asking API to get settings");
  debug("api url = " + settings.api_url + " following route : " + settings.api_usersettings);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_usersettings,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got settings successfully : rapport=" + response.rapport + " rapport_verbose=" + response.rapport_verbose);
    $('#loadData').hide();
    return [true, response.rapport, response.rapport_verbose];
  } catch (error) {
    debug("api_connector : getting settings failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

/**
 *
 * @param {*} frequency
 * @param {*} verbose
 * @returns
 */
export async function SetSettings(frequency, verbose) {
  debug("Asking API to update settings as follow : rapport =" + frequency + " verbose=" + verbose);
  debug("api url = " + settings.api_url + " following route : " + settings.api_usersettings);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  const bodyContent = JSON.stringify({
    "rapport": frequency,
    "rapport_verbose": verbose
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_usersettings,
      method: "POST",
      data: bodyContent,
      headers: headers
    });

    debug("api_connector : updated settings successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : settings update failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}


/**
 *
 * @param {*} frequency
 * @param {*} verbose
 * @returns
 */
export async function GetSMTPList() {
  debug("Asking API to get SMTP list");
  debug("api url = " + settings.api_url + " following route : " + settings.api_smtplist);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_smtplist,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got SMTP list successfully !");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting SMTP list failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

/**
 *
 * @param {*} username
 * @param {*} password
 * @param {*} describe
 * @returns
 */
export async function AddSMTP(username, describe) {
  debug("Asking API to add SMTP auth : name=" + username + " described as=" + describe);
  debug("api url = " + settings.api_url + " following route : " + settings.api_smtpadd);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  const bodyContent = JSON.stringify({
    "username": username,
    "description": describe
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_smtpadd,
      method: "POST",
      data: bodyContent,
      headers: headers
    });

    debug("api_connector : added SMTP auth successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : adding SMTP auth failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

export async function DeleteSMTP(username) {
  debug("Asking API to delete SMTP auth : " + username );
  debug("api url = " + settings.api_url + " following route : " + settings.api_smtpbyusername);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_smtpbyusername + username,
      method: "DELETE",
      headers: headers
    });

    debug("api_connector : deleted SMTP auth successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : deleting SMTP auth failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}


export async function LogStat(dateStart, dateEnd, limit = 10, type = []) {
  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  const content = {
    "dateStart": dateStart,
    "dateEnd": dateEnd,
    "limit": limit,
    "type": type
  };
  debug(headers);
  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_log_stat,
      method: "GET",
      data: content,
      headers: settings.headersList
    });
    debug("api_connector : got SMTP list successfully !");
    debug("response");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting SMTP list failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

/*
  https://app.swaggerhub.com/apis/retzo.net/oMailgw/#/log/get-log-queueId
*/
export async function LogQueueId(queueId) {
  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_log_queueId + queueId,
      method: "GET",
      headers: settings.headersList
    });
    debug("api_connector : got Log QueueId successfully !");
    debug("response");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting Log QueueId failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    $('#loadData_error').html(msgFromAPI);
    $('#loadData_error').show();
    return [false, msgFromAPI, error];
  }
}

/**
 *
 * @param {*} frequency
 * @param {*} verbose
 * @returns
 * Documentation : https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/server/get-server-all-spool
 */
export async function GetServerAllSpool() {
  debug("Asking API to get all spool list");
  debug("api url = " + settings.api_url + " following route : " + settings.api_serverallspool);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_serverallspool,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got spool list successfully !");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting spool list failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}


/*
* Get blacklist
* https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/blacklist
*/
export async function GetBL() {
  debug("Asking API to get blacklist");
  debug("api url = " + settings.api_url + " following route : " + settings.api_blacklist);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_blacklist,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got blacklist successfully !");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting blacklist failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}
/*
* Add blacklist
* https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/blacklist
*/
export async function AddBL(email,status,type) {
  debug("Asking API to add blacklisted : name=" + email + " status=" + status + " type=" + type);
  debug("api url = " + settings.api_url + " following route : " + settings.api_blacklistadd);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  const bodyContent = JSON.stringify({
    "email": email,
    "status": status,
    "type": type
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_blacklistadd,
      method: "POST",
      data: bodyContent,
      headers: headers
    });

    debug("api_connector : added blacklisted successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : adding blacklisted failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}
/*
* Delete blacklist
* https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/blacklist
*/
export async function DeleteBL(username) {
  debug("Asking API to delete blacklisted : " + username );
  debug("api url = " + settings.api_url + " following route : " + settings.api_blacklistdelete);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_blacklistdelete + username,
      method: "DELETE",
      headers: headers
    });

    debug("api_connector : deleted blacklisted successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : deleting blacklisted failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

/**
 *
 * @param {*} frequency
 * @param {*} verbose
 * @returns
 * Documentation : https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/server/get-server-list
 */
export async function GetServerList() {
  debug("Asking API to get all server list");
  debug("api url = " + settings.api_url + " following route : " + settings.api_serverlist);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_serverlist,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got spool list successfully !");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting spool list failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}


/**
 * Get Transport Map
 * Documentation : https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/transport
 */
export async function GetTransport() {
  debug("Asking API to get Transport");
  debug("api url = " + settings.api_url + " following route : " + settings.api_transportlist);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_transportlist,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got transport successfully !");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting blacklist failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}
/*
* Add Transport Map
* Documentation : https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/transport
*/
export async function AddTransport(hostname, search_regex, search, nexthop) {
  debug("Asking API to add transport, data : ");
  let route = settings.api_transportadd.replace("{hostname}", hostname);
  debug("api url = " + settings.api_url + " following route : " + route);
  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  const bodyContent = JSON.stringify({
    "search_regex": search_regex,
    "search": search,
    "nexthop": nexthop
  });
  try {
    const response = await $.ajax({
      url: settings.api_url + route,
      method: "POST",
      data: bodyContent,
      headers: headers
    });
    debug("api_connector : added transport successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : adding transport failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error.responseJSON.message != undefined) {
      debug(msgFromAPI);
      msgFromAPI = error.responseJSON.message;
    }

    return [false, msgFromAPI, error];
  }
}
/*
* Del Transport Map
* Documentation : https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9#/transport
*/
export async function DeleteTransport(id, hostname) {
  debug("Asking API to del transport, data : ");
  debug(hostname);
  var route = settings.api_transportdelete.replace("{hostname}", hostname);
  route = route.replace("{id}", id);
  debug("api url = " + settings.api_url + " following route : " + route);
  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");
  try {
    const response = await $.ajax({
      url: settings.api_url + route,
      method: "DELETE",
      headers: headers
    });
    debug("api_connector : delete transport successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : delete transport failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}
