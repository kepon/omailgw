import { GetBL, AddBL, DeleteBL} from "./api_connector.js";
//;
import showRightInfos from './commun.js';

// Update datas to show
showRightInfos();

setMessage('EMPTY');

jQuery(function() {
  UpdateBLList();
  $("#todaydate").html( moment().format("YYYY-MM-DD") );
});

async function UpdateBLList() {
  // Ask infos from API : Reports Settings
  let getBlacklistTry = await GetBL();
  if (getBlacklistTry[0]) {
    setMessage('EMPTY');
    let Blacklist = getBlacklistTry[1];
    // First foreach adds rows
    $('#Blacklist').html('');
    for (let Blacklisted of Blacklist){
      // translate data into readable content
      // <abbr data-title="No apply to the server for the moment (last cli update : '+moment.unix(server[Transport['hostname']]).format("YYYY-MM-DD HH:MM:SS")+')">
      $('#Blacklist').append(`
        <tr name="row" ${Blacklisted.disableByWhitelist == 0 ? "" : "class='text-secondary'"} data-id="${Blacklisted.id}" data-type="${Blacklisted.type}" data-email="${Blacklisted.email}">
          <td class="text-center">${Blacklisted.type == 1 ? "from" : "to"}</td>
          <td class="text-center" name="email">${Blacklisted.email}</td>
          <td class="text-center" >${Blacklisted.rules}</td>
          <td class="text-center">${Blacklisted.disableByWhitelist == 1 ? "<abbr data-title='Disabled by whitelist'>" : ""} ${Blacklisted.status == 0 ? "Whitelisted" : Blacklisted.status == 1 ? "Greylisted" : "Blacklisted"}${Blacklisted.disableByWhitelist == 1 ? "</abbr>" : ""}</td>
          <td class="text-center">${Blacklisted.rules_count}</td>
          <td class="text-center">${Blacklisted.updated_at}</td>
          <td class="btn-group text-center">
            <button type='button'  data-i18n-key="whitelist" class='btn btn-sm ${Blacklisted.disableByWhitelist == 0 && !Blacklisted.status == 0 ? 'btn-success whitelist-button' : 'text-secondary' }'>
            <i class='fas fa-check me-1'></i>Whitelist</button>
            <button type='button'  data-i18n-key="delete" class='btn btn-sm btn-danger delete-button'>
            <i class='fas fa-trash me-1'></i>Delete</button>
          </td>
        </tr>
      `);
    };
    $( ".whitelist-button" ).on( "click", function() {
      debug("type="+$(this).parent().parent().data('type'));
      debug("email="+$(this).parent().parent().data('email'));
      Add(
        $(this).parent().parent().data('email'),
        0,
        $(this).parent().parent().data('type')
      );
    });
    $( ".delete-button" ).on( "click", function() {
      debug("id="+$(this).parent().parent().data('id'));
      Delete(
        $(this).parent().parent().data('id')
      );
    });
  } else {
    setMessage(getBlacklistTry[2].status + " : " + getBlacklistTry[1]);
  }
}

// Handle deletion action
async function Delete(email) {
  // Show loading spinner
  setMessage('DELETE');
  // Handle the API response
  let deleteTry = await DeleteBL(email);
  if (deleteTry[0]) {
    setMessage('SUCCESS');
    UpdateBLList();
  } else {
    setMessage(deleteTry[2].status + " : " + deleteTry[1]);
  }
};

// Handle adding action
async function Add(email,status,type) {
  // Show loading spinner
  setMessage('ADD');
  // Handle the API response
  let addinTry = await AddBL(
    email,
    status,
    type
  );
  if (addinTry[0]) {
    setMessage('SUCCESS')
    UpdateBLList();
  } else {
    debug(addinTry);
    setMessage(addinTry[2].status + " : " + addinTry[1]);
  }
};

// Listener sur le formulaire d'ajout
$( "#add" ).on( "click", function() {
  Add($('#email').val(), 2, 2);
} );
// Listener su rles input
$('input').keypress(function(e){
  if (e.which == 13) {
    Add($('#email').val(), 2, 2);
  }
});
// Hide alert if click
$( "#alert" ).on( "click", function() {
  $( "#alert" ).hide();
} );


