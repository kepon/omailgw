;

const locationHandler = async () => {
  // get the url path, replace hash with empty string
  const locationHash = window.location.hash.replace("#", "");
  const locationSplit = locationHash.split('?');
  var location = locationSplit[0];
  const locationParam = locationSplit[1];
  debug("location : "+location)
  debug("locationParam : "+locationParam)
  // if the path length is 0, set it to primary page route
  if (location.length == 0) {
      location = "/";
  }
  // get the route object from the routes object
  const route = settings.ui_routes[location] || settings.ui_routes["404"];
  // get the html from the template
  $.get( "template/" + route.template, function( data ) {
    // set the content of the content div to the html
    $( "#layoutSidenav_content main" ).html( data );
    debug( "Load was performed." );
  })
  .fail(function(request, status, error) {
    alert( "Error "+route.template+" : " +error);
  });
  // set the title of the document to the title of the route
  document.title = route.title;
  // set the description of the document to the description of the route
  document
      .querySelector('meta[name="description"]')
      .setAttribute("content", route.description);
  // # Remove old JS
  $( "#routes-includes" ).remove();
  $( "#commun" ).remove();
  $( "#i18n").remove();
  // # Load depend js
  if (route.title != '404') {

    // ****** route-includes
    const script = document.createElement('script');
    script.src = './js/'+route.template.replace(/html/g, "js");
    script.type = 'module';
    script.id = "routes-includes";
    //script.async = true;
    script.onload = () => {
      debug('Routes script loaded successfuly');
    };
    script.onerror = () => {
      debug('Error occurred while loading routes script');
    };

    // ****** commun 
    const scriptCommun = document.createElement('script');
    scriptCommun.src = './js/commun.js';
    scriptCommun.type = 'module';
    scriptCommun.id = "commun";
    scriptCommun.onload = () => {
      debug('Common script loaded successfuly');
    };
    scriptCommun.onerror = () => {
      debug('Error occurred while loading common script');
    };
/*
    // ****** internationalisation
    const scripti18n = document.createElement('script');
    scripti18n.src = './js/i18n.js';
    scripti18n.type = 'module';
    scripti18n.id = "i18n";
    scripti18n.onload = () => {
      debug('Translate script loaded successfuly');
      translateData();
    };
    scripti18n.onerror = () => {
      debug('Error occurred while loading Translate script');
    };

  
    // ****** error messages
    const scriptmsg = document.createElement('script');
    scripti18n.src = './js/messages.js';
    scripti18n.type = 'text/javascript';
    scripti18n.id = "messages";
    scripti18n.onload = () => {
      debug('Error script loaded successfuly');
    };
    scripti18n.onerror = () => {
      debug('Error occurred while loading error script');
    };
*/
    document.body.appendChild(scriptCommun);
    /*
    document.body.appendChild(scripti18n);
    document.body.appendChild(scriptmsg);
    */
    document.body.appendChild(script);
  }
};

const route = (event) => {
  //var beforLocatoin = window.location.hash.replace("#", "");
  event = event || window.event; // get window.event if event argument not provided
  event.preventDefault();
  // window.history.pushState(state, unused, target link);
  window.history.pushState({}, "", event.target.href);
  locationHandler();
};

function changePage(url) {
  debug("Change page: " + url);
  window.location=url;
  location.reload();
}

function hashChangePage() {
  debug(window.location.hash);
  changePage(window.location.hash);
}

jQuery(function() {
  /*
  // create document click that watches the nav links only
  $( "nav a" ).on( "click", function() {
    changePage(this.href)
  });
  */
  // create a function that watches the hash and calls the urlLocationHandler
  window.addEventListener("hashchange", hashChangePage);
  // call the urlLocationHandler to load the page
  locationHandler();
  
  //route();
});
