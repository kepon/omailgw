import { GetSettings, SetSettings, GetSMTPList, AddSMTP, DeleteSMTP } from "./api_connector.js";
;
import showRightInfos from './commun.js';

showRightInfos();

// Update datas to show
jQuery(function() {
  UpdateSettings();
  UpdateSMTPList();
});

// Select html objects
const formRapport = document.querySelector('#usersettingsForm');
const formNew = document.querySelector('#smtpauthForm');
const smtpTable = document.getElementById("smtp-list");

async function UpdateSettings() {
  setMessage('LOAD');
  // Ask infos from API : Reports Settings
  let getSettingsTry = await GetSettings();
  if (getSettingsTry[0]) {
    setMessage('EMPTY');
    formRapport.querySelector('select[name="rapport"]').selectedIndex = getSettingsTry[1];
    formRapport.querySelector('select[name="rapport_verbose"]').selectedIndex = getSettingsTry[2];
  } else {
    setMessage(getSettingsTry[2].status +' : '+ getSettingsTry[1]);
  }
}

async function UpdateSMTPList() {
  setMessage('LOAD');
  // Ask infos from API : Reports Settings
  let getSmtpauthTry = await GetSMTPList();
  if (getSmtpauthTry[0]) {
    setMessage('EMPTY');
    let smtpList = getSmtpauthTry[1];
    // First foreach adds rows
    smtpTable.innerHTML = "";
    for (let smtpAuth of smtpList){
      smtpTable.innerHTML +=
        `
        <tr>
          <td>${smtpAuth.username}</td>
          <td class='password'>
            <span class='passwordHide'>*******</span>
            <span class='passwordShow' style='display:none'>${smtpAuth.password}</span>
          </td>
          <td>${smtpAuth.description}</td>
          <td><button type='button' class='btn btn-danger delete-button' data-i18n-key="delete">Delete</button></td>
        </tr>
        `;

      // Add event listener to handle delete function
      smtpTable.querySelector('.delete-button:last-child').addEventListener('click', () => {
        Delete(smtpAuth.username);
      });
    };
    for (const row of smtpTable.querySelectorAll('tr')) {
      // Add event listener to toggle password display
      const passwordCell = row.querySelector('.password');
      passwordCell.addEventListener('click', () => {
        const passwordHide = passwordCell.querySelector('.passwordHide');
        const passwordShow = passwordCell.querySelector('.passwordShow');
        if (passwordHide.style.display === 'none') {
          passwordHide.style.display = '';
          passwordShow.style.display = 'none';
        } else {
          passwordHide.style.display = 'none';
          passwordShow.style.display = '';
        }
      });
      // Add event listener to the delete button
      row.querySelector('.delete-button').addEventListener('click', () => {
        const usernameCell = row.querySelector('td:first-child');
        const username = usernameCell.textContent.trim();
        Delete(username);
        row.remove(); // Remove the row from the table
      });
    }
  } else {
    setMessage(getSmtpauthTry[2].status +' : '+ getSmtpauthTry[1]);
  }
}

// Handle deletion action
async function Delete(email) {
  setMessage('DELETE');

  // Handle the API response
  let deleteTry = await DeleteSMTP(email);
  if (deleteTry[0]) {
    setMessage('SUCCESS');
    UpdateSMTPList();
  } else {
    setMessage(deleteTry[2].status +' : '+ deleteTry[1]);
  }
};

// Handle Settings Form
formRapport.addEventListener('submit', async (event) => {
  event.preventDefault();
  setMessage('ADD');

  const rapportFrequency = formRapport.querySelector('select[name="rapport"]').value;
  const rapportVerbose = formRapport.querySelector('select[name="rapport_verbose"]').value;

  // Handle the API response
  let setSettingsTry = await SetSettings(rapportFrequency, rapportVerbose);
  if (setSettingsTry[0]) {
    setMessage('SUCCESS');
    UpdateSettings();
  } else {
    setMessage(setSettingsTry[2].status +' : '+ setSettingsTry[1]);
  }
});


// Handle new SMTP auth 'Form' (not a <form> as it is on multiple <th> but behaves like one thanks to multiple listeners)
formNew.addEventListener('submit', async (event) => {
  event.preventDefault();
  setMessage('ADD');

  const email = formNew.querySelector('input[name="newusername"]').value;
  const description = formNew.querySelector('input[name="newdescribe"]').value;

  // Clear previous & show messages
  let errorMessage = "";
  if (email === "") {
    errorMessage += "<span data-i18n-key='mailNNull'>Please enter an email. </span>";
    setMessage(errorMessage);
    return;
  }

  // Handle the API response
  let addAuthTry = await AddSMTP(email, description);
  if (addAuthTry[0]) {
    setMessage('SUCCESS');
    UpdateSMTPList();
  } else {
    setMessage(addAuthTry[2].status +' : '+ addAuthTry[1]);
  }
});

// Listen for "enter" keydown events on the input fields
formNew.querySelector('input[name="newusername"]').addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    event.preventDefault();
    formNew.dispatchEvent(new Event('submit'));
  }
});

formNew.querySelector('input[name="newdescribe"]').addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    event.preventDefault();
    formNew.dispatchEvent(new Event('submit'));
  }
});

// Listen for "click" events on the "Add" button
formNew.querySelector('button[type="submit"]').addEventListener('click', (event) => {
  event.preventDefault();
  formNew.dispatchEvent(new Event('submit'));
});
