import { GetServerAllSpool } from "./api_connector.js";
;
import showRightInfos from './commun.js';

showRightInfos();

async function updateSpooler() {
  setMessage('LOAD');
  let getSpooler = await GetServerAllSpool();
  if (getSpooler[0]) {
    setMessage('EMPTY');
    let dataGetSpoolers = getSpooler[1];
    if (dataGetSpoolers.length != 0) {
      $('#noSpool').hide();
    }
    for (let dataGetSpooler of dataGetSpoolers){
      $('#spools').append(
        '<div class="card mb-4">'+
          '<div class="card-header">'+
            '<div class="row">'+
              '<div class=col>'+
                '<a href="#one?queueId='+dataGetSpooler['queueId']+'">'+dataGetSpooler['queueId']+'</a>'+
              '</div>'+
              '<div class="col text-end text-muted">'+
                dataGetSpooler['hostname']+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="card-body">'+
            '<div class="row">'+
              '<div class=col>'+
                'To : ' + dataGetSpooler['to']+
              '</div>'+
              '<div class="col text-end">'+
                'From : ' + dataGetSpooler['from']+
              '</div>'+
            '</div>'+
            '<div class="row">'+
              '<div class="col">'+
                dataGetSpooler['msg']+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="card-footer small text-muted">'+
            '<label for="date"><span name="date">'+moment.unix(dataGetSpooler['date']).format("YYYY-MM-DD hh:mm:ss")+'</span>'+
          '</div>'+
        '</div>'
      );
    }
    debug(dataGetSpoolers);
  } else {
    setMessage(getSpooler[2].status + " : " + getSpooler[1]);
  }
}
jQuery(function() {
    $(document).ajaxStop(function () {
      $('#loadData').hide();
    });
    updateSpooler()
});
