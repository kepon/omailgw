import { GetSMTPList, noApi, test401, testNormalSyntax } from "./api_connector.js";
;

async function appel200() {
  let getSMTPListTry = await GetSMTPList();
  $("#ajax200return").html(getSMTPListTry[0]);
  return getSMTPListTry[1];
}

$("#ajax200").click(function() {
  debug("ajax200 appelé");
  debug("############ ajax200 RETURN : ");
  debug(appel200());
  debug("/ ############ ajax200 RETURN : ");
});


async function appel401() {
  let get = await test401();
  $("#ajax401return").html(get[0]);
  return get[1];
}
$("#ajax401").click(function() {
  debug("ajax401 appelé");
  debug("############ ajax401 RETURN : ");
  debug(appel401());
  debug("/ ############ ajax401 RETURN : ");
});



async function appelTimeOut() {
  let get = await noApi();
  $("#appelTimeOutreturn").html(get[0]);
  return get[1];
}
$("#appelTimeOut").click(function() {
  debug("appelTimeOut appelé");
  debug("############ appelTimeOut RETURN : ");
  debug(appelTimeOut());
  debug("/ ############ appelTimeOut RETURN : ");
});






async function testNormal() {
  let get = await testNormalSyntax();
  $("#testNormalreturn").html(get[0]);
  return get[1];
}
$("#testNormal").click(function() {
  debug("testNormal appelé");
  debug("############ testNormal RETURN : ");
  debug(testNormal());
  debug("/ ############ testNormal RETURN : ");
});

