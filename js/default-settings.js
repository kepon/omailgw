// *********** NE PAS MODIFIER LES PARAMETRES PAR DEFAUT CI DESSOUS *********
// *********** MODIFIER settings.js *********
// *********** DO NOT MODIFY DEFAULT SETTINGS DOWN HERE **********

const defaultSettings = {
  "debug": false,
  "ui_defaultlanguage": "en-GB",
  "api_url": "https://omailgw.retzo.net/api/",
  // see https://app.swaggerhub.com/apis/retzo.net/oMailgw/0.9
  // for more infos on the API routes
  "api_datatable": "log/dataTables",
  "api_login": "user/login",
  "api_logout": "user/logout",
  "api_changepassword": "user/me/password",
  "api_register": "user/register",
  "api_usersettings": "user/me",
  "api_smtplist": "user/smtp-auth/list",
  "api_smtpadd": "user/smtp-auth/add",
  "api_smtpbyusername": "user/smtp-auth/",
  "api_log_stat": "log/stat",
  "api_log_queueId": "log/",
  "api_adminlist": "admin/user/list",
  "api_adminadd": "admin/user/add",
  "api_adminbyuserid": "admin/user",
  "api_blacklist" : "blacklist/list",
  "api_blacklistadd" :  "blacklist/add",
  "api_blacklistdelete" : "blacklist/",
  "api_serverallspool" : "server/all/spool",
  "api_serverlist" : "server/list",
  "api_transportlist" : "server/all/transport",
  "api_transportadd" : "server/{hostname}/transport/add",
  "api_transportdelete" : "server/{hostname}/transport/{id}",
  "headersList": {
    // retiré pour incompatibilité avec navigateurs (sauf ffox) // "User-Agent": "oMailgw UI (https://framagit.org/omailgw/omailgw/",
    "Accept": "application/json", // Peut être incompatible avec normes CORS
    "Content-Type": "application/json"
  },
  "cookie_duration_days": 30,
  "password_min_length": 8,
  "emailRegex" : /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
  "datatable_date_ago_default": 7,
  "dashboard_percentAllError_danger": 3,
  "dashboard_percentAllError_warning": 1,
  "dashboard_percentInError_danger": 2,
  "dashboard_percentInError_warning": 1,
  "dashboard_percentOutError_danger": 3,
  "dashboard_percentOutError_warning": 1,
  "ui_routes": {
    "404": {
      template: "404.html",
      title: "oMailgw - 404",
      description: "Page not found",
    },
    "500": {
      template: "500.html",
      title: "oMailgw - 500",
      description: "Internal Error",
    },
    "/": {
      template: "log-table.html",
      title: "oMailgw - Log table",
      description: "Log table",
    },
    "dashboard": {
      template: "dashboard.html",
      title: "oMailgw - Dashboard",
      description: "Dashboard - oMailgw",
    },
    "user-settings": {
      template: "user-settings.html",
      title: "oMailgw - User settings",
      description: "Password, Report, SMTP authentification... ",
    },
    "change-password": {
      template: "change-password.html",
      title: "oMailgw - Change password",
      description: "Change password",
    },
    "log-table": {
      template: "log-table.html",
      title: "oMailgw - Log table",
      description: "Log table",
    },
    "one": {
      template: "one.html",
      title: "oMailgw - Message detail",
      description: "Message detail / postcat",
    },
    "root-panel": {
      template: "root-panel.html",
      title: "oMailgw - Root Only",
      description: "Admin rights and other settings",
    },
    "blacklist": {
      template: "blacklist.html",
      title: "oMailgw - Blacklisting",
      description: "emails blacklisted by rules",
    },
    "spool": {
      template: "spool.html",
      title: "oMailgw - Spool",
      description: "emails spooler",
    },
    "server": {
      template: "server.html",
      title: "oMailgw - Server",
      description: "Server détails",
    },
    "transport": {
      template: "transport.html",
      title: "oMailgw - Transport",
      description: "Server transport map ",
    },
  },
  "ui_visibility":{
    "root":{
      sees: "root,admin,user,mailgw"
    },
    "admin":{
      sees: "admin,user"
    },
    "user":{
      sees: "user"
    },
    "mailgw":{
      sees: ""
    }
  }
}

// make settings global
settings = {};
//console.log('settings');
// create settings from default
let keys = Object.keys(defaultSettings);
for (let key of keys){
  settings[key] = defaultSettings[key];

}

if (typeof userSettings == 'undefined') {
  alert("Copy js/settings.js_default to js/settings.js en edit this")
}

//console.log(settings);
// override from user prefs
keys = Object.keys(userSettings);
for (let key of keys){
  settings[key] = userSettings[key];
}
//console.log(settings);
